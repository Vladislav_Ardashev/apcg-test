const API_ROOT = 'https://jsonplaceholder.typicode.com';

const request = (url = '', method = 'GET', data = null) => {
    let options = {
        method: method,
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        }
    };
    if (data) {
        options.body = JSON.stringify(data);
    }
    return fetch(url, options).then(response => response.json());
};

const requests = {
    del: url =>
        request(`${API_ROOT}${url}`, 'DELETE'),
    get: url =>
        request(`${API_ROOT}${url}`, 'GET'),
    put: (url, body) =>
        request(`${API_ROOT}${url}`, 'PUT', body),
    post: (url, body) =>
        request(`${API_ROOT}${url}`, 'POST', body)
};

const Users = {
    all: () => requests.get('/users'),
    get: id => requests.get(`/users/${id}`),
    update: user => requests.put(`/users/${user.id}`, user),
    create: user => requests.post('/users', user),
    delete: id => requests.del(`/users/${id}`)
};

const Posts = {
    all: () => requests.get('/posts'),
    get: id => requests.get(`/posts/${id}`)
};

export default {
    Users,
    Posts
};