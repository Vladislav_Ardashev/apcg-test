import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from "react-router-dom";

import {
    USER_EDITOR_LOADED,
    USER_EDITOR_UNLOADED,
    USER_SUBMITTED,
    UPDATE_FIELD_EDITOR
} from "../../constants/actionTypes";
import api from "../../api";
import Loader from "../common/Loader";

const mapStateToProps = state => ({
    ...state.editor
});

const mapDispatchToProps = dispatch => ({
    onLoad: payload => dispatch({type: USER_EDITOR_LOADED, payload}),
    onSubmit: payload => dispatch({type: USER_SUBMITTED, payload, redirectTo: '/users'}),
    onUpdateField: (key, value) => dispatch({type: UPDATE_FIELD_EDITOR, key, value}),
    onUnload: () => dispatch({type: USER_EDITOR_UNLOADED})
});

class Editor extends Component {

    constructor(props) {
        super(props);

        const updateFieldEvent =
            key => e => this.props.onUpdateField(key, e.target.value);
        this.changeName = updateFieldEvent('name');
        this.changeUsername = updateFieldEvent('username');
        this.changeEmail = updateFieldEvent('email');
        this.changePhone = updateFieldEvent('phone');

        this.submitForm = e => {
            e.preventDefault();
            const user = {
                id: this.props.id,
                name: this.props.name,
                username: this.props.username,
                email: this.props.email,
                phone: this.props.phone
            };
            this.props.onSubmit(
                this.props.mode === 'update'
                    ? api.Users.update(user)
                    : api.Users.create(user)
            );
        };
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        if (id) { // update mode
            this.props.onLoad(api.Users.get(id));
        } else { // create mode
            this.props.onLoad(null);
        }
    }

    componentWillUnmount() {
        this.props.onUnload();
    }

    render() {

        if (!this.props.id && this.props.mode === 'update') {
            return <Loader/>
        }

        return (
            <div className="row justify-content-md-center">
                <div className="col col-lg-6">
                    <h2 className="pt-5">
                        {this.props.id ? `User #${this.props.id}` : 'New user'}
                    </h2>
                    <form onSubmit={this.submitForm}>
                        <div className="mb-3">
                            <input value={this.props.name} className="form-control" placeholder="Name"
                                   onChange={this.changeName} required/>
                        </div>
                        <div className="mb-3">
                            <input value={this.props.username} className="form-control" placeholder="Username"
                                   onChange={this.changeUsername} required/>
                        </div>
                        <div className="mb-3">
                            <input type="email" value={this.props.email} className="form-control" placeholder="Email"
                                   onChange={this.changeEmail} required/>
                        </div>
                        <div className="mb-3">
                            <input value={this.props.phone} className="form-control" placeholder="Phone"
                                   onChange={this.changePhone} required/>
                        </div>
                        <hr className="mb-4"/>
                        <button className="btn btn-primary btn-lg btn-block" type="submit">
                            {this.props.mode === 'update' ? 'Save' : 'Create'}
                        </button>
                    </form>
                </div>
            </div>
        );
    }

}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Editor));
