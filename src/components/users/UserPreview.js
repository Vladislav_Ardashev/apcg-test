import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import api from "../../api";
import {USER_DELETED} from "../../constants/actionTypes";

const mapDispatchToProps = dispatch => ({
    onDelete: (payload, deletedId) => dispatch({type: USER_DELETED, payload, deletedId}),
});

class UserPreview extends Component {

    constructor(props) {
        super(props);

        this.deleteUser = e => {
            e.preventDefault();
            const id = this.props.user.id;
            this.props.onDelete(api.Users.delete(id), id);
        };
    }

    render() {
        return (
            <tr>
                <td>{this.props.user.id}</td>
                <td>{this.props.user.name}</td>
                <td>{this.props.user.username}</td>
                <td>{this.props.user.email}</td>
                <td>{this.props.user.phone}</td>
                <td>
                    <Link to={`/users/${this.props.user.id}`}>Edit </Link>
                    <a href="javascript;" onClick={this.deleteUser}>Del</a>
                </td>
            </tr>
        )
    }
}

export default connect(() => ({}), mapDispatchToProps)(UserPreview);
