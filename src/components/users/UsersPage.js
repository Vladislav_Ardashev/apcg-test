import React from 'react';
import {
    Switch,
    Route,
    useRouteMatch,
    withRouter
} from "react-router-dom";
import UsersList from "./UsersList";
import Editor from "./Editor";

function UsersPage() {
    let match = useRouteMatch();
    return (
        <div>
            <Switch>
                <Route path={`${match.path}/create`}>
                    <Editor mode='create'/>
                </Route>
                <Route path={`${match.path}/:id`}>
                    <Editor mode='update'/>
                </Route>
                <Route path={match.path}>
                    <UsersList/>
                </Route>
            </Switch>
        </div>
    );
}

export default withRouter(UsersPage);
