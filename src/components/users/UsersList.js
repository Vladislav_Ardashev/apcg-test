import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {USERS_PAGE_LOADED, USERS_PAGE_UNLOADED} from '../../constants/actionTypes';
import api from "../../api";
import UserPreview from "./UserPreview";
import Loader from "../common/Loader";

const mapStateToProps = state => ({
    users: state.users
});

const mapDispatchToProps = dispatch => ({
    onLoad: payload => dispatch({type: USERS_PAGE_LOADED, payload}),
    onUnload: () => dispatch({type: USERS_PAGE_UNLOADED})
});

class UsersList extends Component {

    componentDidMount() {
        this.props.onLoad(api.Users.all());
    }

    render() {

        if (!this.props.users.length) {
            return <Loader/>;
        }

        return (
            <div>
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3">
                    <h2 className="pt-3">Users</h2>
                    <div className="btn-toolbar mb-2 mb-md-0">
                        <Link to={'/users/create'} className="btn btn-primary mt-3" role="button">Create</Link>
                    </div>
                </div>

                <div className="table-responsive">
                    <table className="table table-striped table-sm">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Nickname</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.props.users.map((user) => (
                            <UserPreview user={user} key={user.id}/>
                        ))}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersList);