import React, {Component} from 'react'
import {connect} from "react-redux";
import {ALERT_CLOSE} from "../../constants/actionTypes";

const mapStateToProps = state => ({
    ...state.alert
});

const mapDispatchToProps = dispatch => ({
    onClose: () => dispatch({type: ALERT_CLOSE}),
});

class Alert extends Component {
    render() {
        if (this.props.type) {
            return (
                <div className={`alert alert-${this.props.type} my-3`} role="alert">
                    <strong>{this.props.message}</strong>
                    <button type="button" className="close" onClick={this.props.onClose}>
                        <span>&times;</span>
                    </button>
                </div>
            )
        }
        return null;
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Alert);