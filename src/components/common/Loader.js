import React from 'react';

const Loader = () => (
    <div className="loader_wrapper w-100 position-absolute d-flex justify-content-center align-items-center">
        <img src="/load.gif"/>
    </div>
);

export default Loader;