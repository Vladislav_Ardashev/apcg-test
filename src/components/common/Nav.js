import React from 'react';
import {NavLink} from "react-router-dom";

function Nav() {
    return (
        <nav className="col-md-2 d-none d-md-block bg-light sidebar">
            <div className="sidebar-sticky">
                <ul className="nav flex-column">
                    <li className="nav-item">
                        <NavLink to="/" exact={true} className="nav-link" activeClassName="active">Home</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/users" className="nav-link" activeClassName="active">Users</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/posts" className="nav-link" activeClassName="active">Posts</NavLink>
                    </li>
                </ul>
            </div>
        </nav>
    );
}

export default Nav;