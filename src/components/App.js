import React from 'react';
import {
    Switch,
    Route
} from "react-router-dom";
import Header from "./common/Header";
import Nav from "./common/Nav";
import HomePage from "./home/HomePage";
import UsersPage from './users/UsersPage';
import PostsPage from './posts/PostsPage';
import Alert from "./common/Alert";

const App = () => {
    return (
        <div className="App">
            <Header/>
            <div className="container-fluid">
                <div className="row">
                    <Nav/>
                </div>
            </div>
            <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
                <Alert/>
                <Switch>
                    <Route path="/" exact>
                        <HomePage/>
                    </Route>
                    <Route path="/users">
                        <UsersPage/>
                    </Route>
                    <Route path="/posts">
                        <PostsPage/>
                    </Route>
                </Switch>
            </main>
        </div>
    );
};

export default App;
