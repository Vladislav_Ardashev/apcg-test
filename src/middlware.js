import {push} from 'connected-react-router'

const middleware = store => next => action => {
    // check async action
    if (isPromise(action.payload)) {
        // resolve promise
        action.payload.then(
            res => {
                action.payload = res;
                console.log('ACTION', action);
                store.dispatch(action);
                // if we need redirect
                if (action.redirectTo) {
                    store.dispatch(push(action.redirectTo));
                }
            },
            error => {
                console.log('ERROR', error);
                action.error = true;
                action.payload = error.response.body;
                store.dispatch(action);
            }
        );
        return;
    } else if (action.redirectTo) {
        store.dispatch(push(action.redirectTo));
    }
    next(action);
};

function isPromise(v) {
    return v && typeof v.then === 'function';
}

export default middleware;