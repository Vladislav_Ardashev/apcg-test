import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router'
import * as userReducers from './reducers/users';
import * as postsReducers from "./reducers/posts";
import alert from './reducers/alert';

const createRootReducer = (history) => combineReducers({
    ...userReducers,
    ...postsReducers,
    alert,
    router: connectRouter(history)
});

export default createRootReducer