import {
    USER_EDITOR_LOADED,
    USER_EDITOR_UNLOADED,
    USERS_PAGE_LOADED,
    USERS_PAGE_UNLOADED,
    UPDATE_FIELD_EDITOR, USER_DELETED
} from '../constants/actionTypes'

export function editor(state = [], action) {
    switch (action.type) {
        case USER_EDITOR_LOADED:
            return action.payload;
        case UPDATE_FIELD_EDITOR:
            return {...state, [action.key]: action.value};
        case USER_EDITOR_UNLOADED:
            return {};
        default:
            return state;
    }
}

export function users(state = {}, action) {
    switch (action.type) {
        case USERS_PAGE_LOADED:
            return action.payload;
        case USER_DELETED:
            return state.filter(user => user.id !== action.deletedId);
        case USERS_PAGE_UNLOADED:
            return {};
        default:
            return state;
    }
}