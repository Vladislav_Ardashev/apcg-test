import {
    POSTS_PAGE_LOADED
} from '../constants/actionTypes'

export function posts(state = {}, action) {
    switch (action.type) {
        case POSTS_PAGE_LOADED: {
            return action.payload
        }
        default:
            return state;
    }
}