import {
    ALERT_CLOSE,
    USER_EDITOR_LOADED, USER_SUBMITTED
} from '../constants/actionTypes'
import {ALERT_SUCCESS} from "../constants/alertTypes";

export default function (state = {}, action) {
    switch (action.type) {
        case USER_SUBMITTED: {
            return {
                ...state,
                type: ALERT_SUCCESS,
                message: 'User is successfully updated!'
            }
        }
        case ALERT_CLOSE:
            return {};
        default:
            return state;
    }
}