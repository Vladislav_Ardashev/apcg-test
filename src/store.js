import {createStore, applyMiddleware} from "redux";
import middleware from "./middlware";
import {createBrowserHistory} from 'history'
import {routerMiddleware} from 'connected-react-router'
import createRootReducer from "./reducer";

const history = createBrowserHistory();

const store = createStore(
    createRootReducer(history),
    applyMiddleware(
        middleware,
        routerMiddleware(history)
    )
);

export {store, history};